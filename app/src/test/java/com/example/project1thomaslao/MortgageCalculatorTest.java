package com.example.project1thomaslao;

import junit.framework.TestCase;

public class MortgageCalculatorTest extends TestCase {

    public void test_no_tax() {
        double actual = MortgageCalculator.MonthlyPayment(10000.00, 15, 5.5, 0);
        double expected = 81.71f;
        assertEquals(actual, expected, 0.01f);
    }

    public void test_with_tax() {
        double actual = MortgageCalculator.MonthlyPayment(10000.00, 15, 5.5, 0.1);
        double expected = 91.71f;
        assertEquals(actual, expected, 0.01f);
    }

    public void test_with_different_interest_rate_no_taxes() {
        double actual = MortgageCalculator.MonthlyPayment(20000.00, 20, 0, 0);
        double expected = 83.33f;
        assertEquals(actual, expected, 0.01f);
    }

    public void test_with_different_interest_rate_with_taxes() {
        double actual = MortgageCalculator.MonthlyPayment(20000.00, 20, 10.0, 0.1);
        double expected = 213.0f;
        assertEquals(actual, expected, 0.01f);
    }


}