package com.example.project1thomaslao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import com.example.project1thomaslao.databinding.ActivityMainBinding;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private Float interestRate;
    private String progressString;
    private double monthly_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.Uninstall.setOnClickListener(this::onClick);
        binding.Calculate.setOnClickListener(this::onClick);

        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {


                interestRate = (float) (binding.seekBar.getProgress() * 0.1);
//
                progressString = Float.toString(interestRate);
                String interestRate_String = "Interest rate:" + interestRate + "%";
                binding.InterestRate.setText(interestRate_String);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    public void onClick(View view) {

        if (binding.Calculate.isPressed()) {
            if (binding.Principal.getText().length() == 0) {
                binding.Result.setText("Please enter the principle.\nThen press CALCULATE for monthly payments.");
            } else if (BigDecimal.valueOf((Double.parseDouble(binding.Principal.getText().toString()))).scale() > 2) {
                binding.Result.setText("Please enter valid number. 2 decimal digits max.\n Then press CALCULATE for monthly payments!");
            } else {
                if (binding.radioButton.isChecked()) {
                    binding.radioButton2.setChecked(false);
                    binding.radioButton3.setChecked(false);
                    if (binding.taxes.isChecked()) {
                        monthly_payment = MortgageCalculator.MonthlyPayment(Double.parseDouble(binding.Principal.getText().toString()), 15.0, interestRate, 0.1);

                    } else {
                        monthly_payment = MortgageCalculator.MonthlyPayment(Double.parseDouble(binding.Principal.getText().toString()), 15.0, interestRate, 0.0);
                    }
                }

                if (binding.radioButton2.isChecked()) {
                    binding.radioButton.setChecked(false);
                    binding.radioButton3.setChecked(false);
                    if (binding.taxes.isChecked()) {
                        monthly_payment = MortgageCalculator.MonthlyPayment(Double.parseDouble(binding.Principal.getText().toString()), 20.0, interestRate, 0.1);

                    } else {
                        monthly_payment = MortgageCalculator.MonthlyPayment(Double.parseDouble(binding.Principal.getText().toString()), 20.0, interestRate, 0.0);
                    }
                }


                if (binding.radioButton3.isChecked()) {
                    binding.radioButton2.setChecked(false);
                    binding.radioButton.setChecked(false);
                    if (binding.taxes.isChecked()) {
                        monthly_payment = MortgageCalculator.MonthlyPayment(Double.parseDouble(binding.Principal.getText().toString()), 30.0, interestRate, 0.1);
                    } else {
                        monthly_payment = MortgageCalculator.MonthlyPayment(Double.parseDouble(binding.Principal.getText().toString()), 30.0, interestRate, 0.0);
                    }
                }
                binding.Result.setText("$" + Double.toString(monthly_payment));

            }
        }

//            binding.Result.setText(Double.toString(monthly_payment));


        if (binding.Uninstall.isPressed()) {
            Intent delete = new Intent(Intent.ACTION_DELETE,
                    Uri.parse("package:" + getPackageName()));
            startActivity(delete);
        }


    }


}