package com.example.project1thomaslao;

public class MortgageCalculator {

    static double MonthlyPayment(double principle, double numOfYears, double annualInterestRate, double monthlyTaxes) {
//        if (principle < 0) {
//
////            throw new IllegalArgumentException("Must enter a positive principle amount!");
//        }
//        if (numOfYears < 1) {
////            throw new IllegalArgumentException("Enter a valid number of years!");
//        }


        double a = annualInterestRate / 100;
        double monthlyPayment = 0;
        // Monthly Interest
        double J = a / 12;
        double N = numOfYears * 12;
        double T = (monthlyTaxes / 100) * principle;
        if (annualInterestRate == 0.0) {
            monthlyPayment = ((principle) / N) + T;
        } else {
            monthlyPayment = ((principle * J) / (1 - Math.pow((1 + J), -N))) + T;
        }
        return Math.round(monthlyPayment * 100.0) / 100.0;
    }


}
